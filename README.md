# PSONO Client - Password Manager

Master:  [![build status](https://gitlab.com/psono/psono-client/badges/master/build.svg)](https://gitlab.com/psono/psono-client/commits/master) [![coverage report](https://gitlab.com/psono/psono-client/badges/master/coverage.svg)](https://gitlab.com/psono/psono-client/commits/master)  [![Code Climate](https://codeclimate.com/github/psono/psono-client/badges/gpa.svg)](https://codeclimate.com/github/psono/psono-client) [![build status](https://images.microbadger.com/badges/image/psono/psono-client.svg)](https://hub.docker.com/r/psono/psono-client/)  [![build status](https://img.shields.io/docker/pulls/psono/psono-client.svg)](https://hub.docker.com/r/psono/psono-client/)

Develop: [![build status](https://gitlab.com/psono/psono-client/badges/develop/build.svg)](https://gitlab.com/psono/psono-client/commits/develop) [![coverage report](https://gitlab.com/psono/psono-client/badges/develop/coverage.svg)](https://gitlab.com/psono/psono-client/commits/develop)

# Canonical source

The canonical source of PSONO Client is [hosted on GitLab.com](https://gitlab.com/psono/psono-client).

# Documentation

The documentation for the psono server can be found here:

[Psono Documentation](https://doc.psono.com/)

Some things that have not yet found their place in the documentation:

# How to create a release

1. Wait for the build / tests to finish on the develop branch
2. Merge develop branch into master branch
3. Wait for the build / tests to finish on the master branch
4. Create new Tag with the version information e.g v1.0.14 and provide adequate information for the Changelog

    
##